import os
import pandas as pd
import matplotlib.pyplot as plt
#from tabulate import tabulate
#from prettytable import PrettyTable
import numpy as np

filename = "owid-covid-data.csv"

if os.path.exists(filename):
    print("Reading {}".format(filename))
    df = pd.read_csv(filename)
else:
    print("Could not read {}".format(filename))
    exit(0)
    
str2plot = "excess_mortality_cumulative_per_million"
#str2plot = "excess_mortality_cumulative"
#pop = df['population']

cntries = [ "Sweden",
            "Poland"
]

table = {}
header = [ "Country", "Calculated", "Reported", "Quota" ]

fig, ax = plt.subplots(figsize=(16,5))
for h in header:
    print('{:15s} '.format(h), end='')
print("\n")
for c in cntries:
    dff =df[df['location'] == c]
    #p = df[df['population'] == c]
    #y = dff[["date", "population", "total_deaths", str2plot]]
    y = dff[["date", "population", "total_deaths", str2plot]].dropna()
    date = y["date"]
    pop = y["population"]
    ex = y[str2plot]
    yt = pop*ex/1.0e+6
    yd = y["total_deaths"]
    iyd = int(yd.iloc[-1])
    ytv = int(yt.iloc[-1])
    #outstr = "{:15s} {:<15d} {:<15d} {:<15.2f}".format(c, ytv, reported[c], reported[c]/ytv)
    outstr = "{:15s} {:<15d} {:<15d} {:<15.2f}".format(c, ytv, iyd, iyd-ytv)
    #outstr2 = "{} : calc = {} : rep = {} : diff = {}".format(c, ytv, reported[c], reported[c]-ytv)
    outstr2 = "{} : calc = {} : rep = {} : diff = {}".format(c, ytv, iyd, iyd-ytv)
    print(outstr)
    #plt.text(date.iloc[-1],ytv,outstr)
    table[c] = ytv
    plt.plot(date, yt, 'o',label=outstr2)
    #plt.plot(date, yd)
stepsize = 10
start, end = ax.get_xlim()
ax.xaxis.set_ticks(np.arange(start, end, stepsize))

plt.title("https://covid.ourworldindata.org/data/owid-covid-data.csv")
plt.grid()
plt.ylabel("Cumulative Excess Mortality")
plt.legend()
plt.savefig("cumulative_death.png")
plt.show()

#print(tabulate(mydata, headers=head, tablefmt="grid"))
